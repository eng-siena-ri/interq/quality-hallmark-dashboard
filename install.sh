#!/usr/bin/env bash
if [ "$1" == "" ]; then
	echo "Insert the version of your package: '1.0.0'"
	exit 0
fi
docker buildx build --platform linux/amd64 -t interq/quality-hallmark-dashboards:$1 .
#cd ../qhd-service-mockup
#mvn clean package
#docker buildx build --platform linux/amd64 -f Dockerfile -t interq/qhd-service-mockup .
#cd --

cd ../cors-anywhere
docker buildx build --platform linux/amd64 -f Dockerfile -t interq/cors-anywhere .
cd --

