export const environment = {
  production: true,
  // baseUrl: 'http://162.55.94.15:8180/https://tf.interq.inlecom.eu/interq/tf/v1.0/qhs',
  baseUrl: 'http://localhost:6003/http://api:8080/interq/tf/v1.0/qhs',
  useTrustedFramework: true,
};
