import {Component} from '@angular/core';
import {NbLoginComponent} from '@nebular/auth';
import {AppInjector} from '../../../app.module';
import {AUTH_DATA, dialogRef} from '../../../pages/qh-dashboards/qh-dashboards.component';
import {SessionStorageService} from '../../../services/session-storage-service';
import {NbDialogService} from '@nebular/theme';


@Component({
  selector: 'ngx-auth-dialog-component',
  templateUrl: 'auth.dialog.component.html',
  styleUrls: ['auth.dialog.component.scss'],
})
export class NgxAuthDialogComponent extends NbLoginComponent {
  title: String;
  user: any;
  storageService = AppInjector.get(SessionStorageService);

  auth() {
    this.user['notUsed'] = true;
    this.storageService.setData(AUTH_DATA, JSON.stringify(this.user));
    this.close();
  }

  close() {
    dialogRef.state.close();
  }

}

