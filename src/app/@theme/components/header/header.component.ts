import {Component, OnDestroy, OnInit} from '@angular/core';
import {
  NbDialogService,
  NbMediaBreakpointsService,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme';

import {UserData} from '../../../@core/data/users';
import {LayoutService} from '../../../@core/utils';
import {map, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {NgxAuthDialogComponent} from '../auth-dialog/auth.dialog.component';
import {QhdUtils} from '../../../pages/qh-dashboards/qh-utils';
import {SessionStorageService} from '../../../services/session-storage-service';
import {AUTH_DATA, dialogRef} from '../../../pages/qh-dashboards/qh-dashboards.component';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;
  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  currentTheme = 'default';

  userMenu = [
    {title: 'Auth'},
    //  {title: 'Log out'}
  ];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private themeService: NbThemeService,
              private userService: UserData,
              private layoutService: LayoutService,
              private breakpointService: NbMediaBreakpointsService,
              private dialogService: NbDialogService,
              private storageService: SessionStorageService,
  ) {
  }

  ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;

    /*this.userService.getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => this.user = users.nick);*/

    this.storageService.watch(AUTH_DATA)
      .subscribe(authDataStr => {
        if (!QhdUtils.isEmpty(authDataStr)) {
          const authData = JSON.parse(authDataStr);
          this.user = {
            address: authData.address,
            color: authData.notUsed ? 'orange' : 'green',
            style: '',
            picture: authData.notUsed ? 'assets/images/icon-person-orange.jpeg'
              : 'assets/images/icon-person-green.jpeg',
            tooltip: authData.notUsed ? 'Credential Not Verified' : 'Credential Verified',
          };
        } else { // No AUTH data
          this.user = {
            address: 'Anonymous',
            color: 'white',
            tooltip: 'Credential Required',
            picture: 'assets/images/icon-person-gray.jpeg',
          };
        }
      });

    const {xl} = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({name}) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);
    // this.onMenuItemClick();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.storageService.removeData(AUTH_DATA);
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();
    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  onMenuItemClick() {
    this.menuService.onItemClick()
      .subscribe((event) => {
        this.onContextItemSelection(event.item.title);
      });
  }

  onContextItemSelection(title) {
    switch (title) {
      case 'Auth':
       this.openAuthDialog();
        break;

      /*case 'Logout':
        break;*/
    }
  }

  openAuthDialog() {
    const dialogRef_ = this.dialogService.open(NgxAuthDialogComponent,
      {
        hasBackdrop: true,
        hasScroll: true,
        closeOnBackdropClick: true,
      });
    dialogRef.state = dialogRef_;
  }

}
