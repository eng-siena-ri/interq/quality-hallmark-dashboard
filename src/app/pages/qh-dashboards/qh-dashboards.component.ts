import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  NbComponentStatus,
  NbDateService,
  NbDialogService,
  NbIconConfig, NbMenuService, NbSidebarService,
  NbThemeService, NbToastrConfig,
  NbToastrService,
} from '@nebular/theme';
import {FormBuilder, FormControl} from '@angular/forms';
import {RestService} from '../../services/rest.service';
import {LocalDataSource} from 'ng2-smart-table';
import {QhDocument} from './qh-document';
import {QhdParameters} from './qh-parameters';
import {LocalStorageService} from '../../services/local-storage-service';
import {Observable, of, Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {QhdUtils} from './qh-utils';
import {QhdCharts} from './qh-charts';
import {QhdExporter} from './qh-exporter';
import {NgxAuthDialogComponent} from '../../@theme/components/auth-dialog/auth.dialog.component';
import {SessionStorageService} from '../../services/session-storage-service';
import * as dateFns from 'date-fns';
import * as pluginDateFns from 'chartjs-adapter-date-fns';
import {NgxQueryDialogComponent} from './query-dialog/query.dialog.component';
import {RestTfService} from '../../services/rest.tf.service';
import {environment} from '../../../environments/environment';


export const AUTH_DATA = 'auth-data';
export const dialogRef = {
  state: null as any,
};

@Component({
  selector: 'ngx-qh-dashboards',
  styleUrls: ['./qh-dashboards.scss'],
  templateUrl: './qh-dashboards.html',
})
export class QhDashboardsComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  restClient: any;
  dateTimeFormatStandard: string = 'yyyy-MM-dd HH:mm:ss';
  dateFormatStandard: string = 'yyyy-MM-dd';
  dateSuffixTimezoneZFrom: string = 'T00:00:00Z';
  dateSuffixTimezoneZTo: string = 'T23:59:59Z';

  isMeta = true;
  isResult = false;
  isDetail = false;
  isChart = false;
  qhdSelected: QhDocument;
  header: string;
  settings = {
    actions: {
      delete: false,
      add: false,
      edit: false,
    },
    columns: {
      id: {
        title: 'Quality Hallmark ID',
        type: 'string',
        filter: false,
      },
      model: {
        title: 'Model',
        type: 'string',
        filter: false,
      },
      subject: {
        title: 'Subject',
        type: 'string',
        filter: false,
      },
      timeRefFormat: {
        title: 'Time Ref',
        type: 'string',
        filter: false,
      },
    },
  };
  searchByMetaForm: any;
  searchByIdForm: any;
  source: any;
  maxDateFrom: any;
  minDateTo: any;
  private filteredOptions$: Observable<string[]>;
  inputSelQuery: any;
  savedQueriesButtonGroupVisibility: boolean;
  dashboardItems: any;
  @ViewChild('queryInput') queryInput: ElementRef<HTMLInputElement>;

// Charts
  chartPlugins = [pluginDateFns];
  chartsData: any;
  options: any;
  themeSubscription: any;

  constructor(protected dateService: NbDateService<Date>,
              private restClientMock: RestService,
              private restClientTf: RestTfService,
              private formBuilder: FormBuilder,
              private localStorageService: LocalStorageService,
              private sessionStorageService: SessionStorageService,
              private toastService: NbToastrService,
              private dialogService: NbDialogService,
              private themeService: NbThemeService,
              private menuService: NbMenuService,
  ) {
    if (environment.useTrustedFramework) {
      this.restClient = this.restClientTf;
    } else {
      this.restClient = this.restClientMock;
    }
  }

  ngOnInit() {
    this.searchByMetaForm = this.formBuilder.group({
      owner: '',
      asset: '',
      model: '',
      subject: '',
      timeRefFrom: '',
      timeRefTo: '',
    });
    this.searchByIdForm = this.formBuilder.group({
      id: '',
    });
    this.source = new LocalDataSource();
    this.minDateTo = this.searchByMetaForm.get('timeRefFrom').value;
    this.maxDateFrom = this.searchByMetaForm.get('timeRefTo').value;
    this.inputSelQuery = new FormControl('');
    this.themeSubscription = this.themeService.getJsTheme().subscribe(config => {
      const chartjs: any = config.variables.chartjs;

      this.options = {
        maintainAspectRatio: false,
        responsive: true,
        plugins: {
          legend: {
            position: 'top',
            align: 'start',
            labels: {
              boxWidth: 0,
              color: chartjs.textColor,
            },
          },
          tooltip: {
            enabled: true,
            usePointStyle: true,
            callbacks: {
              label: (tooltipItem) => tooltipItem.parsed.y,
            },
          },
        },
        scales: {
          x:
            {
              /*
              type: 'timeseries',
               time: {
                 displayFormats: {
                   day: 'dd/MM',
                 },
                 tooltipFormat: this.dateTimeFormatStandard,
               },
               gridLines: {
                 display: false,
                 color: chartjs.axisLineColor,
               },
               ticks: {
                 fontColor: chartjs.textColor,
               },
               */
              display: false,
            },
          y:
            {
              beginAtZero: false,
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
        },
      };
    });
    this.changeSR('S'); // Meta
    this.isMeta = true;
    this.filteredOptions$ = of(this.localStorageService.getAllKeys());
    this.themeService.onThemeChange()
      .pipe(
        map(({name}) => name),
        takeUntil(this.destroy$),
      );
    this.savedQueriesButtonGroupVisibility = false;
    this.dashboardItems = [
      {
        title: 'Standard Dashboard',
        icon: 'bar-chart-outline',
        active: true,
      },
      {
        title: 'Custom Dashboard 1',
        active: false,
      },
      {
        title: 'Custom Dashboard 2',
        active: false,
      },
      {
        title: 'Custom Dashboard 3',
        active: false,
      },
    ];
    this.onMenuItemClick();
    this.minDateTo = null;
    this.maxDateFrom = null;
    setTimeout(() => {
      this.queryInput.nativeElement.blur();
    }, 50);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.themeSubscription.unsubscribe();
  }


  onViewAction(event): void {
    this.qhdSelected = event.data;
    this.isDetail = true;
    this.isResult = false;
  }

  changeSR(event: String) {
    this.isResult = false;
    if (event === 'S') {
      this.isMeta = true;
      this.header = 'Search QH repository';
      this.source.load([]);
      this.isDetail = false;
    } else {
      this.isMeta = false;
      this.header = 'Retrieve QH by ID';
      this.source.load([]);
      this.isDetail = false;
    }
  }

  buildParamsFromForm() {
    const owner = this.searchByMetaForm.get('owner').value;
    const asset = this.searchByMetaForm.get('asset').value;
    const model = this.searchByMetaForm.get('model').value;
    const subject = this.searchByMetaForm.get('subject').value;
    let from = this.searchByMetaForm.get('timeRefFrom').value;
    let to = this.searchByMetaForm.get('timeRefTo').value;
    if (dateFns.isDate(from))
      from = dateFns.format(from, this.dateFormatStandard) + this.dateSuffixTimezoneZFrom;
    if (dateFns.isDate(to))
      to = dateFns.format(to, this.dateFormatStandard) + this.dateSuffixTimezoneZTo;
    if (QhdUtils.isEmpty(owner) &&
      QhdUtils.isEmpty(asset) &&
      QhdUtils.isEmpty(model) &&
      QhdUtils.isEmpty(subject) &&
      QhdUtils.isEmpty(from) &&
      QhdUtils.isEmpty(to)) {
      return null;
    }
    const params: QhdParameters = {
      owner: owner,
      asset: asset,
      model: model,
      subject: subject,
      from: from,
      to: to,
    };
    return params;
  }

  buildFormMetaFromParams(query: string) {
    if (QhdUtils.isEmpty(query)) return;
    const params = JSON.parse(this.localStorageService.getData(query));
    this.searchByMetaForm.reset();
    if (!QhdUtils.isEmpty(params.owner))
      this.searchByMetaForm.get('owner').setValue(params.owner);
    if (!QhdUtils.isEmpty(params.asset))
      this.searchByMetaForm.get('asset').setValue(params.asset);
    if (!QhdUtils.isEmpty(params.model))
      this.searchByMetaForm.get('model').setValue(params.model);
    if (!QhdUtils.isEmpty(params.subject))
      this.searchByMetaForm.get('subject').setValue(params.subject);
    if (!QhdUtils.isEmpty(params.from))
      this.searchByMetaForm.get('timeRefFrom').setValue(params.from);
    if (!QhdUtils.isEmpty(params.to))
      this.searchByMetaForm.get('timeRefTo').setValue(params.to);
  }


  loadMeta(): void {
    const owner = this.searchByMetaForm.get('owner').value;
    const asset = this.searchByMetaForm.get('asset').value;
    const model = this.searchByMetaForm.get('model').value;
    const subject = this.searchByMetaForm.get('subject').value;
    if (QhdUtils.isEmpty(owner) &&
      QhdUtils.isEmpty(asset) &&
      QhdUtils.isEmpty(model) &&
      QhdUtils.isEmpty(subject)) {
      this.showToast('Please fill at least one field! One of Owner, Asset, Model or Subject!', 'warning');
      return;
    }
    this.verifyAuth();
    const qhdParameters = this.buildParamsFromForm();
    this.restClient.getDocumentsByMeta(qhdParameters)
      .subscribe(qhd => {
          this.source.empty();
          let qhDataset = this.buildQHDocs(qhd);
          qhDataset = qhDataset.sort(function (a, b) {
            return new Date(a.timeRef) > new Date(b.timeRef) ? 1 : a.timeRef < b.timeRef ? -1 : 0;
          });
          this.source.load(qhDataset);
          const authData = JSON.parse(this.sessionStorageService.getData(AUTH_DATA));
          authData.notUsed = false;
          this.sessionStorageService.setData(AUTH_DATA, JSON.stringify(authData));
          this.isResult = true;
        },
        error => {
          this.showToast(QhdUtils.formatErrorMessage(error), 'danger');
          if (error.status === 401 || error.status === 402 || error.status === 403)
            this.showAuthDialog(true);
        });
  }

  resetMeta() {
    this.isDetail = false;
    this.isResult = false;
    this.searchByMetaForm.reset();
    this.source.load([]);
    this.inputSelQuery.setValue('');
    this.queryInput.nativeElement.blur();
    this.minDateTo = null;
    this.maxDateFrom = null;
  }

  loadId() {
    this.verifyAuth();
    const idValue = this.searchByIdForm.get('id').value;
    if (QhdUtils.isEmpty(idValue)) {
      this.showToast('Please enter a valid ID', 'warning');
      return;
    }
    this.restClient.getDocumentById(idValue)
      .subscribe(qhd => {
          const qhdRetrieved = this.buildQHDocument(qhd);
          qhdRetrieved.id = idValue;
          this.source.empty();
          this.source.load([qhdRetrieved]);
          const authData = JSON.parse(this.sessionStorageService.getData(AUTH_DATA));
          authData.notUsed = false;
          this.sessionStorageService.setData(AUTH_DATA, JSON.stringify(authData));
          this.qhdSelected = qhdRetrieved;
          this.isDetail = true;
          this.isResult = false;
        },
        error => {
          this.showToast(QhdUtils.formatErrorMessage(error), 'danger');
          if (error.status === 401 || error.status === 402 || error.status === 403) {
            this.showAuthDialog(true);
          }
        });
  }

  verifyAuth() {
    const authData = this.sessionStorageService.getData(AUTH_DATA);
    if (QhdUtils.isEmpty(authData)) {
      this.showAuthDialog(false);
    } else {
      this.restClient.authData = JSON.parse(authData);
    }
  }

  showAuthDialog(isError: boolean) {
    if (isError)
      this.sessionStorageService.setData(AUTH_DATA, null);
    else {
      const authDataStr = this.sessionStorageService.getData(AUTH_DATA);
      if (!QhdUtils.isEmpty(authDataStr)) {
        const authData = JSON.parse(authDataStr);
        authData.notUsed = false;
        this.sessionStorageService.setData(AUTH_DATA, JSON.stringify(authData));
      }
      dialogRef.state = this.dialogService.open(NgxAuthDialogComponent,
        {
          hasBackdrop: true,
          hasScroll: true,
          closeOnBackdropClick: true,
        });
    }
  }

  resetId() {
    this.isDetail = false;
    this.isResult = false;
    this.searchByIdForm.reset();
    this.source.load([]);
  }

  buildQHDocs(qhds: any) {
    let qhd;
    const docs = [];
    for (qhd of qhds) {
      docs.push(this.buildQHDocument(qhd));
    }
    return docs;
  }

  buildQHDocument(qhd: any) {
    const header = qhd['qhd-header'];
    const document: QhDocument = {
      id: qhd.id ?? qhd['qhd-id'],
      owner: header.owner,
      subject: header.subject,
      asset: header.asset,
      model: header.model,
      timeRef: header.timeRef ?? header.timeref,
      timeRefFormat: dateFns.format(new Date(header.timeRef ?? header.timeref), this.dateTimeFormatStandard),
      body: qhd['qhd-body'],
      bodyObj: qhd['qhd-body'].payload || qhd['qhd-body'].key ?
        JSON.parse(qhd['qhd-body'].payload ?? qhd['qhd-body'].key) : qhd['qhd-body'],
    };
    return document;
  }

  goToSearchByMeta() {
    this.isDetail = false;
    this.isResult = true;
    this.isMeta = true;
    this.isChart = false;
    setTimeout(() => {
      this.queryInput.nativeElement.blur();
    }, 50);
  }

  goToSearchById() {
    this.isDetail = false;
    this.isResult = false;
    this.isMeta = false;
    this.isChart = false;
  }

  selectPeriod(period: string) {
    const end = 30;
    this.searchByMetaForm.get('timeRefTo').setValue(new Date().toISOString().slice(0, end));
    switch (period) {
      case '1y':
        this.searchByMetaForm.get('timeRefFrom').setValue(this.dateService.addYear(new Date(), -1)
          .toISOString().slice(0, end));
        break;
      case '1m':
        this.searchByMetaForm.get('timeRefFrom').setValue(this.dateService.addMonth(new Date(), -1)
          .toISOString().slice(0, end));
        break;
      case '1w':
        this.searchByMetaForm.get('timeRefFrom').setValue(this.dateService.addDay(new Date(), -7)
          .toISOString().slice(0, end));
        break;
      case '1d':
        this.searchByMetaForm.get('timeRefFrom').setValue(this.dateService.addDay(new Date(), -1)
          .toISOString().slice(0, end));
        break;
      case '1h':
        this.searchByMetaForm.get('timeRefFrom').setValue(this.dateService.addHours(new Date(), -1)
          .toISOString().slice(0, end));
        break;
      default:
        this.searchByMetaForm.get('timeRefFrom').setValue(this.dateService.addDay(new Date(), -7)
          .toISOString().slice(0, end));
        break;
    }
  }

  saveQuery() {
    const parameters = this.buildParamsFromForm();
    if (parameters == null) {
      this.showToast('Cannot save an empty query!', 'warning');
      return;
    }
    /*if (!QhdUtils.isEmpty(this.localStorageService.getData(this.inputSelQuery.value))) {
      this.showToast('Impossible to save query! Query already exists!', 'warning');
      return;
    }*/
    this.dialogService.open(NgxQueryDialogComponent,
      {
        hasBackdrop: true,
        hasScroll: true,
        closeOnBackdropClick: true,
        context: {
          function: 'S',
          header: 'Save query',
          queryInput: this.inputSelQuery,
          queryInputNative: this.queryInput.nativeElement,
          data: parameters,
        },
      });
  }


  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.localStorageService.getAllKeys()
      .filter(optionValue => optionValue.toLowerCase().includes(filterValue)
        && !optionValue.includes(AUTH_DATA));
  }

  getFilteredOptions(value: string): Observable<string[]> {
    return of(value).pipe(
      map(filterString => this.filter(filterString)),
    );
  }

  onChange($event) {
    if (QhdUtils.isEmpty($event.target.value)) {
      this.savedQueriesButtonGroupVisibility = false;
    }
    this.filteredOptions$ = this.getFilteredOptions(this.inputSelQuery.value);
    this.filteredOptions$.subscribe(values => {
      if (values[0] === $event)
        this.savedQueriesButtonGroupVisibility = true;
      else
        this.savedQueriesButtonGroupVisibility = false;
    });
  }

  onSelectionChange($event) {
    if (QhdUtils.isEmpty($event)) {
      this.savedQueriesButtonGroupVisibility = false;
    }
    this.filteredOptions$ = this.getFilteredOptions($event);
    this.filteredOptions$.subscribe(values => {
      if (values[0] === $event)
        this.savedQueriesButtonGroupVisibility = true;
      else
        this.savedQueriesButtonGroupVisibility = false;
    });
    this.buildFormMetaFromParams($event);
  }

  showToast(message: string, status: NbComponentStatus) {
    let title = status.toString();
    let icon = 'checkmark-circle-outline';
    if (status === 'warning')
      icon = 'alert-triangle-outline';
    else if (status === 'danger') {
      icon = 'alert-circle-outline';
      title = 'error';
    }
    title = title.charAt(0).toUpperCase() + title.slice(1);
    const config = {
      icon: {icon: icon, pack: 'eva'},
      duration: 30000,
      status: status,
    };
    this.toastService.show(message, title, config);
  }

  generateChart() {
    this.isChart = true;
    this.isResult = false;
    this.isDetail = false;
    const charts = new QhdCharts(this.themeService);
    this.chartsData = charts.formatData(this.source.data);
  }

  exportExcel() {
    const charts = new QhdCharts(this.themeService);
    let chartsData = charts.formatData(this.source.data);
    chartsData = JSON.parse(JSON.stringify(chartsData));
    const datesOfInterval = chartsData[0].labels;
    const excelData = [];
    for (let i = 0; i < datesOfInterval.length; i++) {
      const row = {};
      row['date'] = datesOfInterval[i];
      for (const chart of chartsData) {
        const dataset = chart.datasets[0];
        row[dataset.label] = dataset.data[i];
      }
      excelData.push(row);
    }
    QhdExporter.exportExcel(excelData);
  }

  exportPdf(qhd: QhDocument) {
    QhdExporter.exportPdf(qhd);
  }

  deleteSavedQuery() {
    this.dialogService.open(NgxQueryDialogComponent,
      {
        hasBackdrop: true,
        hasScroll: true,
        closeOnBackdropClick: true,
        context: {
          function: 'D',
          header: 'Remove query',
          queryInput: this.inputSelQuery,
          queryInputNative: this.queryInput.nativeElement,
        },
      });
  }

  renameSavedQuery() {
    this.dialogService.open(NgxQueryDialogComponent,
      {
        hasBackdrop: true,
        hasScroll: true,
        closeOnBackdropClick: true,
        context: {
          function: 'R',
          header: 'Rename query',
          queryInput: this.inputSelQuery,
          queryInputNative: this.queryInput.nativeElement,
        },
      });
  }

  onMenuItemClick() {
    this.menuService.onItemClick()
      .subscribe((event) => {
        this.onContextItemSelection(event.item.title);
      });
  }

  onContextItemSelection(title) {
    switch (title) {
      case 'Standard Dashboard':
        this.generateChart();
        break;
    }
  }

  dateChangeFrom($event: any) {
    this.maxDateFrom = new Date($event);
  }

  dateChangeTo($event: any) {
    this.minDateTo = new Date($event);
  }
}
