import {Pipe, PipeTransform} from '@angular/core';
import {INDICATOR_PREFIX} from './qh-charts';

@Pipe({
  name: 'prettyprint',
})
export class PrettyPrintPipe implements PipeTransform {

  transform(val) {
    const value = JSON.stringify(val, undefined, 4)
        .replace(new RegExp('{', 'g'), '')
        .replace(new RegExp('}', 'g'), '')
        .replace(new RegExp(';', 'g'), '')
        .replace(new RegExp(',', 'g'), '')
        .replace(new RegExp('"', 'g'), '')
      // .replace(/^\s*[\r\n]/gm, '')
    ;
    const allLines = value.split(/\r\n|\n/);
    let ret = '';
    allLines.forEach((line) => {
      const lineFormatted = this.makeIndicatorStrong(line);
      if (lineFormatted !== '' &&
        lineFormatted.trim() !== '' &&
        lineFormatted.trimLeft() !== '' &&
        lineFormatted.trimRight() !== '')
        ret += lineFormatted + '\n';
    });
    document.getElementById('qhd-body-formatted').innerHTML = ret;
  }

  private makeIndicatorStrong(line: string) {
    if (line.length === 0) return '';
    const position = line.search(INDICATOR_PREFIX);
    let size = 0;
    let word = '';
    if (line.includes(INDICATOR_PREFIX)) {
      while (line.charAt(position + size) !== ':') {
        word += line.charAt(position + size);
        size++;
      }
      const wordStrong = '<strong>' + word + '</strong>';
      return line.replace(word, wordStrong);
    } else
      return line;
  }
}
