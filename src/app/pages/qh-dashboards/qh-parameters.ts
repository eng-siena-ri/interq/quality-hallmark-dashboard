export class QhdParameters {
  model: string;
  asset: string;
  owner: string;
  subject: string;
  from: string;
  to: string;
}
