import * as dateFns from 'date-fns';
import {NbColorHelper, NbThemeService} from '@nebular/theme';

export const INDICATOR_PREFIX = 'IND';

export class QhdCharts {
  indicators: any;
  dateTimeFormatStandard: string = 'yyyy-MM-dd HH:mm:ss';

  constructor(private themeService: NbThemeService) {
  }

  formatData(source: any) {
    const sourceSorted = source.sort(function (a, b) {
      return new Date(a.timeRef) > new Date(b.timeRef) ? 1 : a.timeRef < b.timeRef ? -1 : 0;
    });
    const intervalBegin = sourceSorted[0].timeRef;
    const intervalEnd = sourceSorted[sourceSorted.length - 1].timeRef;
    this.indicators = [];
    for (const qhd of sourceSorted) {
      const body = this.flatten(qhd.bodyObj);
      this.findRecursive(qhd, body);
    }
    const indicatorsSorted = this.indicators.sort(function (a, b) {
      const nameA = a.name;
      const nameB = b.name;
      return (nameA < nameB) ? -1 : (nameA > nameB) ? 1 : 0;
    });

    const datesGenerated = dateFns.eachDayOfInterval({
      start: new Date(intervalBegin),
      end: new Date(intervalEnd),
    });

    const datesOfInterval = [];
    for (const dateTime of datesGenerated) {
      const dateSx = dateFns.format(dateTime, 'yyyy-MM-dd');
      const find = sourceSorted.filter(v => v.timeRef.substring(0, 10) === dateSx);
      if (typeof find === 'undefined' || find.length === 0) {
        datesOfInterval.push(dateTime);
      } else {
        find.forEach(v => datesOfInterval.push(new Date(v.timeRef)));
      }
    }

    const chartsData = [];
    const indicatorsByName = this.groupBy(indicatorsSorted, 'name');
    const entries = Object.entries(indicatorsByName);
    for (const [key, values] of entries) {
      const dataSorted = JSON.parse(JSON.stringify(values));
      this.insertNullValuesForNoDates(datesOfInterval, dataSorted, key);
      const dataSorted_ = dataSorted.sort(function (a, b) {
        return new Date(a.timeRef) > new Date(b.timeRef) ? 1 : a.timeRef < b.timeRef ? -1 : 0;
      });
      this.themeService.getJsTheme().subscribe(config => {
        const chartData = this.getChartData(datesOfInterval, dataSorted_, key, config.variables);
        chartsData.push(chartData);
      });
    }

    return chartsData;
  }

  private insertNullValuesForNoDates(datesOfInterval: any[], dataSorted, key: string) {
    for (const date of datesOfInterval) {
      const dateNoTime = dateFns.format(date, this.dateTimeFormatStandard);
      const find = dataSorted.find(v => {
        const dateNoTime2 = dateFns.format(new Date(v.timeRef), this.dateTimeFormatStandard);
        return dateNoTime2 === dateNoTime;
      });
      if (typeof find === 'undefined') {
        dataSorted.push({
          name: key,
          value: null,
          timeRef: dateFns.format(date, this.dateTimeFormatStandard),
        });
      }
    }
  }

  private getChartData(datesOfInterval: Date[], dataSorted: any[], label: string, colors: any) {
    return {
      labels: datesOfInterval,
      // .map(d => d.toISOString().substring(0, 10)), // 2022-03-02
      datasets: [{
        data: dataSorted.map(v => v.value == null ? null : +v.value),
        label: label,
        backgroundColor: NbColorHelper.hexToRgbA(colors.primaryLight, 0.8),
      }],
    };
  }

  private findRecursive(qhd, obj) {
    const entries = Object.entries(obj);
    for (const [key, value] of entries) {
      if (typeof value !== 'object') {
        if (key.includes(INDICATOR_PREFIX))
          this.indicators.push({
            name: key,
            value: value,
            timeRef: qhd.timeRef,
          });
      } else {
        this.findRecursive(qhd, value);
      }
    }
  }

  private flatten(obj: Record<string, unknown>, parent?: string): Record<string, unknown> {
    let res: Record<string, unknown> = {};
    for (const [key, value] of Object.entries(obj)) {
      const propName = parent ? parent + '/' + key : key;
      const flattened: Record<string, unknown> = {};
      if (value instanceof Date) {
        flattened[key] = value.toISOString();
      } else if (typeof value === 'object' && value !== null) {
        res = {...res, ...this.flatten(value as Record<string, unknown>, propName)};
      } else {
        res[propName] = value;
      }
    }
    return res;
  }

  private groupBy<T extends Record<string, any>, K extends keyof T>(
    array: T[],
    key: K | { (obj: T) },
  ): Record<string, T[]> {
    const keyFn = key instanceof Function ? key : (obj: T) => obj[key];
    return array.reduce((objectsByKeyValue, obj) => {
      const value = keyFn(obj);
      objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
      return objectsByKeyValue;
    }, {} as Record<string, T[]>);
  }
}
