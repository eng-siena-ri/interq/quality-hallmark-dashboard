import {Component} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {LocalStorageService} from '../../../services/local-storage-service';
import {QhdUtils} from '../qh-utils';

@Component({
  selector: 'ngx-query-dialog-component',
  templateUrl: 'query.dialog.component.html',
  styleUrls: ['query.dialog.component.scss'],
})
export class NgxQueryDialogComponent {
  function: string;
  queryInput: any;
  queryInputNative: any;
  header: string;
  data: any;
  errors: string[] = [];
  messages: string[] = [];
  queryName: any;

  constructor(protected dialogQRef: NbDialogRef<any>, private localStorageService: LocalStorageService) {
    this.function = 'S';
    this.queryInput = '';
    this.header = '';
  }


  delete() {
    const data1 = this.localStorageService.getData(this.queryInput.value);
    if (QhdUtils.isEmpty(data1)) {
      this.errors.push('Query name ' + this.queryInput.value + ' not found!');
      return;
    }
    this.localStorageService.removeData(this.queryInput.value);
    this.dialogQRef.close('remove');
    this.queryInput.setValue('', {emitEvent: true});
    this.queryInputNative.blur();

  }

  rename() {
    if (QhdUtils.isEmpty(this.queryInput.value)) {
      this.errors.push('Query name is empty!');
      return;
    }
    const originalName = JSON.parse(JSON.stringify(this.queryInput.value));
    const data = this.localStorageService.getData(originalName);
    const data1 = this.localStorageService.getData(this.queryName);
    if (!QhdUtils.isEmpty(data1)) {
      this.errors.push('Query name ' + this.queryName + ' already exists!');
      return;
    }
    this.localStorageService.setData(this.queryName, data);
    this.localStorageService.removeData(originalName);
    this.dialogQRef.close('rename');
    this.queryInput.setValue(this.queryName, {emitEvent: true});
    this.queryInputNative.blur();

  }

  save() {
    if (QhdUtils.isEmpty(this.queryInput.value)) {
      this.errors.push('Query name is empty!');
      return;
    }
    const data = this.localStorageService.getData(this.queryInput.value);
    if (!QhdUtils.isEmpty(data)) {
      this.errors.push('Query name ' + this.queryInput.value + ' already exists!');
      return;
    }
    this.localStorageService.setData(this.queryInput.value, JSON.stringify(this.data));
    this.dialogQRef.close('save');
    this.queryInput.setValue(this.queryInput.value, {emitEvent: true});
    this.queryInputNative.blur();
  }

  close() {
    this.dialogQRef.close();
  }

  emptyMessages() {
    this.messages = [];
    this.errors = [];
  }
}

