import * as XLSX from 'xlsx';
import jsPDF from 'jspdf';
import {QhDocument} from './qh-document';
import * as dateFns from 'date-fns';
const doc = new jsPDF();


export class QhdExporter {
  private static fileName: string = 'qhds';


  public static exportExcel(source: any): void {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(source, {});
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName + '-' + dateFns.format(new Date(), 'dd-MM-yyyy') + '.xlsx');
  }

  public static exportPdf(qhd: QhDocument): void {
    const x = 5;
    let y = 10;
    doc.text('QH: ' + qhd.id, x, y);
    y = y + 10;
    for (let [key, value] of Object.entries(qhd)) {
      if (key === 'id') continue;
      if (key === 'body') continue;
      if (key === 'timeRef') continue;
      if (key === 'timeRefFormat') {
        key = 'timeRef';
      }
      if (key === 'bodyObj') {
        key = '';
        value = JSON.stringify(value, undefined, 4)
          .replace(new RegExp('{', 'g'), '')
          .replace(new RegExp('}', 'g'), '')
          .replace(new RegExp(';', 'g'), '')
          .replace(new RegExp(',', 'g'), '')
          .replace(new RegExp('"', 'g'), '');
      }
      doc.text(this.capitalizeFirstLetter(key) + value, x, y);
      y = y + 10;
    }
    doc.save('qhd_' + qhd.id + '.pdf');
  }

  private static capitalizeFirstLetter(string) {
    if (string === '') return string;
    return string.charAt(0).toUpperCase() + string.slice(1) + ': ';
  }

}
