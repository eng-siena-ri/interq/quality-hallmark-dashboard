export interface QhdBody {
  payload: string;
}

export interface QhDocument {
  id: string;
  owner: string;
  asset: string;
  model: string;
  subject: string;
  timeRef: Date;
  timeRefFormat: string;
  body: QhdBody;
  bodyObj: any;
}

