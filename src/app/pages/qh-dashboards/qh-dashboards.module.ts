import {NgModule} from '@angular/core';
import {
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbDatepickerModule, NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    NbLayoutModule,
    NbCalendarRangeModule,
    NbCalendarModule,
    NbAutocompleteModule,
    NbTooltipModule, NbAlertModule, NbContextMenuModule,
} from '@nebular/theme';

import {
  NbDateFnsDateModule,
} from '@nebular/date-fns';

import {ThemeModule} from '../../@theme/theme.module';
import {QhDashboardsComponent} from './qh-dashboards.component';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PrettyPrintPipe} from './pretty-print.pipe';
import {NgChartsModule} from 'ng2-charts';
import {NgxQueryDialogComponent} from './query-dialog/query.dialog.component';


@NgModule({
    imports: [
        ThemeModule,
        NbInputModule,
        NbCardModule,
        NbButtonModule,
        NbActionsModule,
        NbUserModule,
        NbCheckboxModule,
        NbRadioModule,
        NbDatepickerModule,
        NbSelectModule,
        NbIconModule,
        NbLayoutModule,
        NbCalendarRangeModule,
        NbCalendarModule,
        Ng2SmartTableModule,
        ReactiveFormsModule,
        NbAutocompleteModule,
        NbTooltipModule,
        NbDateFnsDateModule,
        NgChartsModule,
        FormsModule,
        NbAlertModule,
        NbContextMenuModule,
    ],
  declarations: [
    QhDashboardsComponent,
    PrettyPrintPipe,
    NgxQueryDialogComponent,
  ],
})
export class QhDashboardsModule {
}
