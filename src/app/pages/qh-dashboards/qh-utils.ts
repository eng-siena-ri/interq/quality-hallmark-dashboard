export class QhdUtils {
  public static isEmpty = (val: any) => val == null || !(Object.keys(val) || val).length;

  public static formatErrorMessage = (error: any) => {
    let message = '';
    if (error.error && error.error.error && error.error.error.message) {
      message = error.error.error.message;
    } else if (error.error && error.error.message) {
      message = error.error.message;
    } else if (error.message) {
      message = error.message;
    } else {
      message = error.toString();
    }
    const errorMessage =  error.status ? `Http Status: ${error.status} - ${message}` : message;
    console.error(errorMessage);
    return errorMessage;
  }

}
