import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {PagesComponent} from './pages.component';
import {QhDashboardsComponent} from './qh-dashboards/qh-dashboards.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'qh-dashboards',
      component: QhDashboardsComponent,
    },
    {
      path: '',
      redirectTo: 'qh-dashboards',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
