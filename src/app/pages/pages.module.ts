import {NgModule} from '@angular/core';
import {NbMenuModule} from '@nebular/theme';

import {ThemeModule} from '../@theme/theme.module';
import {PagesComponent} from './pages.component';
import {PagesRoutingModule} from './pages-routing.module';
import {QhDashboardsModule} from './qh-dashboards/qh-dashboards.module';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    QhDashboardsModule,
  ],
  declarations: [
    PagesComponent,
  ],
})
export class PagesModule {
}
