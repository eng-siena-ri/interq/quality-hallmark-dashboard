import {NbMenuItem} from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'QHD',
    icon: 'pie-chart-outline',
    link: '/pages/qh-dashboards',
  },
];
