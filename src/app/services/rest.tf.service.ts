import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {QhdParameters} from '../pages/qh-dashboards/qh-parameters';
import {NbToastrService} from '@nebular/theme';
import {QhdUtils} from '../pages/qh-dashboards/qh-utils';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RestTfService {
  public baseUrl = environment.baseUrl;
  authData: any;

  constructor(private httpClient: HttpClient) {
  }

  public getDocuments(): Observable<any> {
    return this.httpClient.get(this.baseUrl);
  }
  public getDocumentsByMeta(parameters: QhdParameters): Observable<Object> {
    let params = new HttpParams();
    params = params.append('cid', this.authData.address);
    params = params.append('pwd', this.authData.password);
    if (this.isEmpty(parameters))
      return this.httpClient.get(`${this.baseUrl}/`, {params});
    if (!this.isEmpty(parameters.from))
      params = params.append('tf', parameters.from);
    if (!this.isEmpty(parameters.to))
      params = params.append('tt', parameters.to);
    if (!this.isEmpty(parameters.owner))
      params = params.append('o', parameters.owner);
    if (!this.isEmpty(parameters.model))
      params = params.append('m', parameters.model);
    if (!this.isEmpty(parameters.subject))
      params = params.append('s', parameters.subject);
    if (!this.isEmpty(parameters.asset))
      params = params.append('a', parameters.asset);
    return this.httpClient.get(`${this.baseUrl}/`, {params});
  }

  public getDocumentById(id: number): Observable<Object> {
    let params = new HttpParams();
    params = params.append('cid', this.authData.address);
    params = params.append('pwd', this.authData.password);

    return this.httpClient.get(`${this.baseUrl}/${id}`, {params});
  }


  public isEmpty(obj) {
    return QhdUtils.isEmpty(obj);
  }

}

