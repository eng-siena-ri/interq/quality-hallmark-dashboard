import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SessionStorageService {

  private subjects: Map<string, BehaviorSubject<any>>;

  constructor() {
    this.subjects = new Map<string, BehaviorSubject<any>>();
    for (const [key, value] of Object.entries(sessionStorage)) {
      this.subjects.set(key, new BehaviorSubject(value));
    }
  }

  getData(key: string): any {
     return sessionStorage.getItem(key);
  }

  getAllKeys(): any {
    return Object.keys(sessionStorage);
  }

  setData(key: string, value: any): any {
    if (key && value) {
      sessionStorage.setItem(key, value);
    }
    if (!this.subjects.has(key)) {
      this.subjects.set(key, new BehaviorSubject<any>(value));
    } else {
      this.subjects.get(key).next(value);
    }
  }

  removeData(key: string) {
    if (this.subjects.has(key)) {
      this.subjects.get(key).complete();
      this.subjects.delete(key);
    }
    sessionStorage.removeItem(key);
  }

  removeStorage() {
    sessionStorage.removeStorage();
    this.subjects.clear();
  }

  watch(key: string): Observable<any> {
    if (!this.subjects.has(key)) {
      this.subjects.set(key, new BehaviorSubject<any>(null));
    }
    const item = sessionStorage.getItem(key);
    this.subjects.get(key).next(item);
    return this.subjects.get(key).asObservable();
  }

}


