import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

// import {LocalStorage, StorageMap} from '@ngx-pwa/local-storage';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {

  private subjects: Map<string, BehaviorSubject<any>>;

  constructor() {
    this.subjects = new Map<string, BehaviorSubject<any>>();
  }

  getData(key: string): any {
    return localStorage.getItem(key);
  }

  getAllKeys(): any {
    return Object.keys(localStorage);
  }

  setData(key: string, value: any): any {
    if (key && value) {
      localStorage.setItem(key, value);
    }
    if (!this.subjects.has(key)) {
      this.subjects.set(key, new BehaviorSubject<any>(value));
    } else {
      this.subjects.get(key).next(value);
    }
  }

  removeData(key: string) {
    if (this.subjects.has(key)) {
      this.subjects.get(key).complete();
      this.subjects.delete(key);
    }
    localStorage.removeItem(key);
  }

  removeStorage() {
    localStorage.removeStorage();
    this.subjects.clear();
  }

  watch(key: string): Observable<any> {
    if (!this.subjects.has(key)) {
      this.subjects.set(key, new BehaviorSubject<any>(null));
    }
    const item = localStorage.getItem(key);
    this.subjects.get(key).next(item);
    return this.subjects.get(key).asObservable();
  }

}


