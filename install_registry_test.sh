#!/usr/bin/env bash
docker buildx build --platform linux/amd64 -t registry.gitlab.com/eng-siena-ri/interq/quality-hallmark-dashboard:test .
docker login registry.gitlab.com
docker push registry.gitlab.com/eng-siena-ri/interq/quality-hallmark-dashboard:test
