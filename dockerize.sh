#!/usr/bin/env bash
if [ "$1" == "" ]; then
	echo "Insert the version of your package: '1.0.0'"
	exit 0
fi
docker build -f Dockerfile -t interq/quality-hallmark-dashboards:$1 .
