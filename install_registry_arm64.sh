#!/usr/bin/env bash
docker buildx build --platform linux/arm64/v8 -t registry.gitlab.com/eng-siena-ri/interq/quality-hallmark-dashboard:arm64 .
docker login registry.gitlab.com
docker push registry.gitlab.com/eng-siena-ri/interq/quality-hallmark-dashboard:arm64
